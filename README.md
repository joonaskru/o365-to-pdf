# Office to pdf

## Dependencies and set up
This module uses MSGraph API to:
- Upload the document
- Download the document in pdf format
- Delete the uploaded document

To make this work, you need to create an app in azure active directory (https://docs.microsoft.com/en-us/graph/tutorials/node?tutorial-step=2).
To this app, you need to create required permissions, and as far as i know, it should be enough to register
Files.ReadWrite.All in the site you are using as the formating tmp.

In sharepoint, you need to create a site/a new folder in existing site, that will be used as the upload tmp folder.

When you have both these, you should apply configs in /src/config.ts

Under settings, you tell the folderID that we will use as the tmp and siteID(sharepoint site id).

In authSettings you type in the application secret, application Id and tenant.

After these steps, everything should be up and running.

## Test that connectivity is working

Run "npm test" and check the output. Should tell you where you can find  document that was converted.
/src/tests/test.ts can be used as an code example for module usage.