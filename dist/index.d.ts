import "isomorphic-fetch";
/**
 * Basic usage "example". With the original file in your drive, you can convert it to PDF.
 *
 * Feel free to modify and create new operations as per your needs
 *
 * @returns Promise with a string to the newfly create pdf file in your local drive. Absolute path
 *
 * @param filePath Filepath of file that needs to be converted
 * @param fileName Filename of the file to be converted
 * @param pdfPath Path where the pdf will be written to
 */
export default function docxToPdf(filePath: string, fileName: string, pdfPath: string): Promise<string>;
