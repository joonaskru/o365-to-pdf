"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
require("isomorphic-fetch");
const auth_1 = require("./auth");
const client = __importStar(require("./client"));
/**
 * Basic usage "example". With the original file in your drive, you can convert it to PDF.
 *
 * Feel free to modify and create new operations as per your needs
 *
 * @returns Promise with a string to the newfly create pdf file in your local drive. Absolute path
 *
 * @param filePath Filepath of file that needs to be converted
 * @param fileName Filename of the file to be converted
 * @param pdfPath Path where the pdf will be written to
 */
function docxToPdf(filePath, fileName, pdfPath) {
    return new Promise((resolve, reject) => {
        auth_1.getAccessToken().then((token) => {
            client.uploadDocument(token, filePath, fileName).then((docId) => {
                client.downloadPdfDocument(token, docId, pdfPath).then((docPath) => {
                    client.deleteDriveItem(token, docId).then((deleted) => {
                        resolve(pdfPath);
                    }).catch((err) => {
                        reject(err);
                    });
                }).catch((err) => {
                    reject(err);
                });
            }).catch((err) => {
                reject(err);
            });
        }).catch((err) => {
            reject(err);
        });
    });
}
exports.default = docxToPdf;
//# sourceMappingURL=index.js.map