"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Custom build authentication provider
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const Tenant = config_1.authSettings.Tenant;
const AppId = config_1.authSettings.AppId;
const AppSecret = config_1.authSettings.AppSecret;
const endpoint = "https://login.microsoftonline.com/" + Tenant + ".onmicrosoft.com/oauth2/v2.0/token";
const requestParams = {
    client_id: AppId,
    client_secret: AppSecret,
    grant_type: "client_credentials",
    scope: "https://graph.microsoft.com/.default",
};
function getAccessToken() {
    return new Promise((resolve, reject) => {
        request_1.default.post({ url: endpoint, form: requestParams }, (err, response, body) => {
            if (err) {
                reject(err);
            }
            else {
                const parsedBody = JSON.parse(body);
                if (parsedBody.error_description) {
                    reject(parsedBody.error_description);
                }
                else {
                    resolve(parsedBody.access_token);
                }
            }
        });
    });
}
exports.getAccessToken = getAccessToken;
//# sourceMappingURL=auth.js.map