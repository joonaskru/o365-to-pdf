/**
 * Download existing item in sharepoint as PDF and write it to drive
 *
 * @returns Promise that will resolve to the PDF's path once ready with writing
 *
 * @param token Bearer toke from authentication module
 * @param documentId Id of the document in sharepoint. This can be obtained by uploadDocument,
 * which returns a promise that will resolve in to document id of uploaded item.
 * @param pdfPath Path where pdf will be written. Currently only supports writing right away
 */
export declare function downloadPdfDocument(token: string, documentId: string, pdfPath: string): Promise<string>;
/**
 * Upload a document to predefined sharepoint folder.
 *
 * Uses low-level implementation of MS Graph API.
 *
 * @returns Promise<string> Promises a string, that represents the newly uploaded fileId in sharepoint.
 *
 * @param token Bearer token from authentication module
 * @param filePath Absolute file path of the document that we want to upload
 * @param fileName Name of the uploaded file
 */
export declare function uploadDocument(token: string, filePath: string, fileName: string): Promise<string>;
/**
 * Function to delete a file from sharepoint drive
 *
 * @returns Promise for a boolean. True if the operation was succesfull. Other vice it will reject
 *
 * @param token Bearer token. Resolved from authentication module
 * @param documentId Document you want to delete from sharepoint drive
 */
export declare function deleteDriveItem(token: string, documentId: string): Promise<boolean>;
