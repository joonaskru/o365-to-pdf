"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
/**
 * Download existing item in sharepoint as PDF and write it to drive
 *
 * @returns Promise that will resolve to the PDF's path once ready with writing
 *
 * @param token Bearer toke from authentication module
 * @param documentId Id of the document in sharepoint. This can be obtained by uploadDocument,
 * which returns a promise that will resolve in to document id of uploaded item.
 * @param pdfPath Path where pdf will be written. Currently only supports writing right away
 */
function downloadPdfDocument(token, documentId, pdfPath) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        const r = request_1.default.get("https://graph.microsoft.com/v1.0/drive/items/" + documentId + "/content?format=pdf", {
            auth: {
                bearer: token,
            },
        }, (err, response, body) => {
            if (err) {
                reject(err);
            }
            else if (body.error) {
                reject(body.error.message);
            }
        }).pipe(fs_1.default.createWriteStream(path_1.default.join(pdfPath)));
        r.on("close", () => {
            resolve(pdfPath);
        });
    }));
}
exports.downloadPdfDocument = downloadPdfDocument;
/**
 * Upload a document to predefined sharepoint folder.
 *
 * Uses low-level implementation of MS Graph API.
 *
 * @returns Promise<string> Promises a string, that represents the newly uploaded fileId in sharepoint.
 *
 * @param token Bearer token from authentication module
 * @param filePath Absolute file path of the document that we want to upload
 * @param fileName Name of the uploaded file
 */
function uploadDocument(token, filePath, fileName) {
    return new Promise((resolve, reject) => {
        request_1.default.put("https://graph.microsoft.com/v1.0/sites/" + config_1.settings.siteId + "/" +
            "drive/items/" + config_1.settings.parentFolder + ":/" + fileName + ":/content", {
            auth: {
                bearer: token,
            },
            body: fs_1.default.createReadStream(filePath),
        }, (err, response, body) => {
            const parsedBody = JSON.parse(body);
            if (err) {
                reject(err);
            }
            else if (parsedBody.error) {
                reject(parsedBody.error.message);
            }
            else {
                resolve(parsedBody.id);
            }
        });
    });
}
exports.uploadDocument = uploadDocument;
/**
 * Function to delete a file from sharepoint drive
 *
 * @returns Promise for a boolean. True if the operation was succesfull. Other vice it will reject
 *
 * @param token Bearer token. Resolved from authentication module
 * @param documentId Document you want to delete from sharepoint drive
 */
function deleteDriveItem(token, documentId) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        const r = request_1.default.delete("https://graph.microsoft.com/v1.0/sites/" + config_1.settings.siteId + "/drive/items/" + documentId, {
            auth: {
                bearer: token,
            },
        }, (err, response, body) => {
            const parsedBody = JSON.parse(body);
            reject("Ebiiin2)");
            if (err) {
                reject(err);
            }
            else if (parsedBody.error) {
                reject(parsedBody.error.message);
            }
            else {
                resolve(true);
            }
        });
    }));
}
exports.deleteDriveItem = deleteDriveItem;
//# sourceMappingURL=client.js.map