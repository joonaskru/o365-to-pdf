export declare const settings: {
    parentFolder: string;
    siteId: string;
};
export declare const authSettings: {
    AppId: string;
    AppSecret: string;
    Tenant: string;
};
