"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../dist/client");
const auth_1 = require("../dist/auth");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
describe("Test different parts of the program", () => {
    test('Test authentication', (done) => {
        auth_1.getAccessToken().then((token) => {
            expect(token.length).toBeGreaterThan(20);
            done();
        }).catch((err) => {
            throw new Error(err);
        });
    }, 20000);
    let docId;
    const testDocxPath = path_1.default.join(__dirname, "test.docx");
    const testPdfPath = path_1.default.join(__dirname, "test.pdf");
    test('Test document upload', (done) => {
        auth_1.getAccessToken().then((token) => {
            client_1.uploadDocument(token, testDocxPath, "test.docx").then((documentId) => {
                expect(documentId.length).toBeGreaterThan(10);
                docId = documentId;
                done();
            }).catch((err) => {
                throw new Error(err);
            });
        });
    });
    test("test downloading as a pdf", (done) => {
        auth_1.getAccessToken().then((token) => {
            client_1.downloadPdfDocument(token, docId, testPdfPath).then((result) => {
                const pdfStats = fs_1.default.statSync(testPdfPath);
                expect(pdfStats.size).toBeGreaterThan(47128);
                expect(pdfStats.size).toBeLessThan(51128);
                done();
                fs_1.default.unlink(testPdfPath, (err) => {
                    if (err) {
                        throw new Error("Could not delete created PDF file");
                    }
                });
            }).catch((err) => {
                throw new Error(err);
            });
        });
    });
    test("test remove file from the sharepoint", (done) => {
        auth_1.getAccessToken().then((token) => {
            client_1.deleteDriveItem(token, docId).then((res) => {
                expect(res).toBe(true);
            }).catch((err) => {
                throw new Error(err);
            });
        });
    });
});
//# sourceMappingURL=maintest.test.js.map