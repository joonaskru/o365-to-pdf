"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = __importDefault(require("../index"));
const path_1 = __importDefault(require("path"));
const testDocx = path_1.default.join(__dirname, "../../testDocs/test.docx");
const testPdf = path_1.default.join(__dirname, "../../testDocs/test.pdf");
index_1.default(testDocx, "test.docx", testPdf).then((filePath) => {
    console.log("Test ran, output can be found from " + filePath);
});
//# sourceMappingURL=test.js.map