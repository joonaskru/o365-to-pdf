// Custom build authentication provider
import request from "request";
import { authSettings } from "./config";

const Tenant = authSettings.Tenant;
const AppId = authSettings.AppId;
const AppSecret = authSettings.AppSecret;

const endpoint = "https://login.microsoftonline.com/" + Tenant + ".onmicrosoft.com/oauth2/v2.0/token";
const requestParams = {
	client_id: AppId,
	client_secret: AppSecret,
	grant_type: "client_credentials",
	scope: "https://graph.microsoft.com/.default",
};

/**
 * Proimise to return bearer token for configured application user
 *
 * @returns Promise<string>, where string is the bearer token.
 */
export function getAccessToken(): Promise<string> {
	return new Promise((resolve, reject) => {
		request.post({ url: endpoint, form: requestParams }, (err, response, body) => {
			if (err) {
				reject(err);
			} else {
				const parsedBody = JSON.parse(body);
				if (parsedBody.error_description) {
					reject(parsedBody.error_description);
				} else {
					resolve(parsedBody.access_token);
				}
			}
		});
	});
}
