export const settings = {
	parentFolder: "SHAREPOINT FOLDER ID",
	siteId: "SHAREPOINT SITE ID",
};

export const authSettings = {
	AppId: "YOURAPPLICATIONIDHERE",
	AppSecret: "APPSECRETHERE",
	Tenant: "YOURTENANT",
};
