import "isomorphic-fetch";
import path from "path";
import { getAccessToken } from "./auth";
import * as client from "./client";

/**
 * Basic usage "example". With the original file in your drive, you can convert it to PDF.
 *
 * Feel free to modify and create new operations as per your needs
 *
 * @returns Promise with a string to the newfly create pdf file in your local drive. Absolute path
 *
 * @param filePath Filepath of file that needs to be converted
 * @param fileName Filename of the file to be converted
 * @param pdfPath Path where the pdf will be written to
 */
export default function docxToPdf(filePath: string, fileName: string, pdfPath: string): Promise<string> {
	return new Promise((resolve, reject) => {
		getAccessToken().then((token) => {
			client.uploadDocument(token, filePath, fileName).then((docId) => {
				client.downloadPdfDocument(token, docId, pdfPath).then((docPath) => {
					client.deleteDriveItem(token, docId).then((deleted) => {
						resolve(pdfPath);
					}).catch((err) =>{
						reject(err);
					});
				}).catch((err) =>{
					reject(err);
				});
			}).catch((err) =>{
				reject(err);
			});
		}).catch((err) =>{
			reject(err);
		});
	});
}
