import docToPdf from "../index";
import path from "path";

const testDocx = path.join(__dirname, "../../testDocs/test.docx");
const testPdf = path.join(__dirname, "../../testDocs/test.pdf");

docToPdf(testDocx, "test.docx", testPdf).then((filePath: string) =>{
	console.log("Test ran, output can be found from " + filePath);
})
